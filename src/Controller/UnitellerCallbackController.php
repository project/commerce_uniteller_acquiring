<?php

namespace Drupal\commerce_uniteller_acquiring\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;

/**
 * Controller routines for page example routes.
 */
class UnitellerCallbackController extends ControllerBase {

  public function callback(Request $request) {
    $logger = \Drupal::logger('commerce_uniteller_acquiring');
    $config_name = 'commerce_payment.commerce_payment_gateway.plugin.uniteller_acquiring';
    $password = \Drupal::config($config_name)->get('password');
    $password = '7hYoVTxYzCcBa2CRLvbiLQolnMWcATmL5mCzCQ8rgApCT6OgFpxjZMg4qbUp3baULgNX9fcO6PWKWXuD';
    $signature = $request->request->get('Signature');
    $order_id = $request->request->get('Order_ID');
    $status = $request->request->get('Status');
    if(!empty($order_id)) {
      $entityTypeManager = \Drupal::entityTypeManager();
      $order = $entityTypeManager->getStorage('commerce_order')
        ->load($order_id);

      $payment = $entityTypeManager->getStorage('commerce_payment')
        ->loadByRemoteId($order_id);

      $payment_gateway_plugin = $payment->getPaymentGateway()->getPlugin();
      $configs = $payment_gateway_plugin->getConfiguration();
      $password = $configs['password'];

      $control_signature = strtoupper(md5($order_id . $status . $password));

      if($control_signature === $signature) {
        $logger->info('Подпись Signature совпала.');

        if($status === 'paid' || $status === 'authorized') {
          if($status === 'paid') {
            $payment->setState('completed');
            $payment->setAmount($order->getTotalPrice());
          }
          else
            $payment->setState('authorization');
          $payment->setRemoteState($status);
          $payment->setCompletedTime(time());
        }
        elseif($status === 'partly canceled')
          $payment->setState('partially_refunded');
        else
          $payment->setState('authorization_voided');
        $payment->save();
      }
      else {
        $logger->info('Подпись Signature не совпала. Signature: @Signature. Control Signature: @control_signature', ['@Signature' => $signature, '@control_signature' => $control_signature]);
        return false;
      }
    }
    else {
      $logger->info('Uniteller returned empty request!');
      return false;
    }

    return [
      '#markup' => '<p>' . $this->t('Simple page: The quick brown fox jumps over the lazy dog.') . '</p>',
    ];
  }

}
