<?php

namespace Drupal\commerce_uniteller_acquiring\PluginForm\OffsiteRedirect;

use Drupal;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm as BasePaymentOffsiteForm;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Order registration and redirection to payment URL.
 */
class UnitellerAcquiringForm extends BasePaymentOffsiteForm implements ContainerInjectionInterface {

  /**
   * The currency storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  private $currencyStorage;

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  private $httpClient;

  /**
   * The current language.
   *
   * @var \Drupal\Core\Language\LanguageInterface
   */
  private $currentLanguage;

  /**
   * The logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  private $logger;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * UnitellerAcquiringForm constructor.
   *
   * @param \Drupal\Core\Entity\EntityStorageInterface $currency_storage
   *   The currency storage.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The HTTP client.
   * @param \Drupal\Core\Language\LanguageInterface $current_language
   *   The current language.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   *   The logger channel.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(
    EntityStorageInterface $currency_storage,
    ClientInterface $http_client,
    LanguageInterface $current_language,
    LoggerChannelInterface $logger,
    ModuleHandlerInterface $module_handler
  ) {
    $this->currencyStorage = $currency_storage;
    $this->httpClient = $http_client;
    $this->currentLanguage = $current_language;
    $this->logger = $logger;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')->getStorage('commerce_currency'),
      $container->get('http_client'),
      $container->get('language_manager')->getCurrentLanguage(),
      $container->get('logger.channel.commerce_uniteller_acquiring'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;
    // If payment is not have ID, we force save it, since it used in $order_id
    // which is required for payment.
    if ($payment->isNew()) {
      $payment->save();
    }

    /** @var \Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayInterface $payment_gateway_plugin */
    $payment_gateway_plugin = $payment->getPaymentGateway()->getPlugin();
    $configs = $payment_gateway_plugin->getConfiguration();
    // Get password for payment method.
    $password = $configs['password'];
    $shop_id = $configs['point_id'];
    $field_phone = $configs['field_phone'];

    $api_uri = 'https://fpay.uniteller.ru/v2/api/register';

    date_default_timezone_set('Europe/Moscow');
    $current_date = date('Y-m-d H:i:s', time());

    $order = $payment->getOrder();
    $order_id = $payment->getOrderId();
    $order_total = $payment->getAmount()->getNumber();
    $order_products = $order->getItems();
    foreach ($order_products as $key => $order_product) {
      $lines[$key] = [
        'name' => $order_product->getPurchasedEntity()->label(),
        'price' => round($order_product->getUnitPrice()->getNumber(), 2),
        'qty' => (int) $order_product->getQuantity(),
        'sum' => round($order_product->getTotalPrice()->getNumber(), 2),
        'vat' => 20,
        'payattr' => 1,
        'lineattr' => 1,
      ];
    }

    $customer = $order->getCustomer();
    $receipt_array = [
      'customer' => [
        'phone' => $customer->get('field_phone')->value,
        'email' => $customer->getEmail(),
        'id' => $customer->id(),
      ],
      'taxmode' => 0,
      'lines' => $lines,
      'payments' => [
        [
          'kind' => 1,
          'type' => 0,
          'amount' => round($order_total, 2),
        ]
      ],
      'total' => round($order_total, 2),
    ];
    $receipt = base64_encode(json_encode($receipt_array));

    $signature_data = hash('sha256', $order_id) . '&' .
      hash('sha256', $shop_id) . '&' .
      hash('sha256', '') . '&' .
      hash('sha256', $current_date) . '&' .
      hash('sha256', $receipt) . '&' .
      hash('sha256', $password);
    $signature = strtoupper(hash('sha256', $signature_data));

    // Prepare params to be executed.
    $params = http_build_query([
      'OrderID' => $order_id,
      'UPID' => $shop_id,
      'OrderLifeTime' => '',
      'CurrentDate' => $current_date,
      'IsRecurrentStart' => 0,
      'Receipt' => $receipt,
      'Signature' => $signature,
      'URL_RETURN_OK' => $form['#return_url'],
      'URL_RETURN_NO' => $form['#cancel_url'],
    ]);
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $api_uri);
    curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/x-www-form-urlencoded']);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_VERBOSE, 0);
    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLINFO_HEADER_OUT, 1);
    $response = curl_exec($ch);
    $curl_error = curl_error($ch);
    curl_close($ch);

    $data = json_decode($response);
    if($data->Code === '00' || $data->Code === '04') {
      $payment_url = $data->Link;

      $payment->setAuthorizedTime(time());
      $payment->setRemoteId($data->OrderID);
      $payment->setState('new');
      $payment->save();

      return $this->buildRedirectForm(
        $form,
        $form_state,
        $payment_url,
        [],
        self::REDIRECT_POST
      );
    }
    else {
      // If something goes wrong, we stop payment and show error for it.
      $this->logger->error("Payment for order #@order_id is throws an error. Code: @code. Note: @note.", [
        '@order_id' => $payment->getOrderId(),
        '@code' => $data->Code,
        '@note' => $data->Note,
      ]);

      // Mark payment is failed.
      $payment->setState('authorization_voided');
      $payment->save();

      throw new PaymentGatewayException();
    }
  }

}
