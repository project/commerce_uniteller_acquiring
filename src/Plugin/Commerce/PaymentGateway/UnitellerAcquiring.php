<?php

namespace Drupal\commerce_uniteller_acquiring\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\PaymentMethodTypeManager;
use Drupal\commerce_payment\PaymentTypeManager;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use GuzzleHttp\ClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Url;

/**
 * Provides the Uniteller Acquiring payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "uniteller_acquiring",
 *   label = @Translation("Uniteller Acquiring"),
 *   display_label = @Translation("Uniteller"),
 *   forms = {
 *     "offsite-payment" = "Drupal\commerce_uniteller_acquiring\PluginForm\OffsiteRedirect\UnitellerAcquiringForm",
 *   },
 *   payment_method_types = {"credit_card"},
 *   requires_billing_information = FALSE,
 *   credit_card_types = {
 *     "maestro", "mastercard", "visa", "mir",
 *   },
 * )
 */
class UnitellerAcquiring extends OffsitePaymentGatewayBase implements ContainerFactoryPluginInterface {

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * Constructs a new UnitellerAcquiring object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\commerce_payment\PaymentTypeManager $payment_type_manager
   *   The payment type manager.
   * @param \Drupal\commerce_payment\PaymentMethodTypeManager $payment_method_type_manager
   *   The payment method type manager.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The http client.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    PaymentTypeManager $payment_type_manager,
    PaymentMethodTypeManager $payment_method_type_manager,
    TimeInterface $time,
    ClientInterface $http_client
  ) {

    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $payment_type_manager, $payment_method_type_manager, $time);

    $this->httpClient = $http_client;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.commerce_payment_type'),
      $container->get('plugin.manager.commerce_payment_method_type'),
      $container->get('datetime.time'),
      $container->get('http_client')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'username' => '',
      'password' => '',
      'order_id_prefix' => '',
      'order_id_suffix' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['username'] = [
      '#type' => 'textfield',
      '#title' => $this->t("Username"),
      '#required' => TRUE,
      '#default_value' => $this->configuration['username'],
    ];

    $form['password'] = [
      '#type' => 'password',
      '#title' => $this->t("Password"),
      '#description' => $this->t("Password stored in database. To change it, enter new password, or leave field empty and password won't change."),
      '#default_value' => $this->configuration['password'],
    ];

    $form['point_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t("Uniteller Point ID"),
      '#required' => TRUE,
      '#default_value' => $this->configuration['point_id'],
    ];

    $form['ip'] = [
      '#type' => 'textarea',
      '#title' => $this->t("Uniteller IP-addresses"),
      '#required' => TRUE,
      '#default_value' => $this->configuration['ip'],
      '#description' => 'Введите IP-адреса через запятую, без переносов.'
    ];

    $form['field_phone'] = [
      '#type' => 'textfield',
      '#title' => $this->t("Customer field phone."),
      '#required' => TRUE,
      '#default_value' => $this->configuration['field_phone'],
    ];

    $url = Url::fromRoute('commerce_uniteller_acquiring.callback');
    $url->setAbsolute();
    $string = $url->toString();
    $form['url'] = [
      '#type' => 'markup',
      '#markup' => $string,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValue($form['#parents']);
    if ($values['password'] == '' && $this->configuration['password'] == '') {
      $form_state->setError($form['password'], $this->t("Password field is required."));
    }
    if ($values['point_id'] == '' && $this->configuration['point_id'] == '') {
      $form_state->setError($form['password'], $this->t("Password field is required."));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    // Parent method will reset configuration array and further condition will
    // fail. So we temporary store old password before configuration was erased.
    $current_password = $this->configuration['password'];
    parent::submitConfigurationForm($form, $form_state);
    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['username'] = $values['username'];
      $this->configuration['point_id'] = $values['point_id'];
      $this->configuration['ip'] = $values['ip'];
      $this->configuration['field_phone'] = $values['field_phone'];
      // Handle password saving.
      if ($values['password'] != '') {
        $this->configuration['password'] = $values['password'];
      }
      else {
        $this->configuration['password'] = $current_password;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function onReturn(OrderInterface $order, Request $request) {
    $logger = \Drupal::logger('commerce_uniteller_acquiring');
    $logger->info('Request onReturn');

    $order_id = $request->query->get('Order_ID');
    $entityTypeManager = \Drupal::entityTypeManager();
    $payment = $entityTypeManager->getStorage('commerce_payment')
      ->loadByRemoteId($order_id);

    if(!in_array($payment->getState(), ['new', 'authorization', 'completed']))
      throw new PaymentGatewayException('Payment failed!');
  }

}
