<?php

namespace Drupal\commerce_uniteller_acquiring;

use Symfony\Component\HttpFoundation\Request;

class Uniteller {

  const API_URI      = 'https://securepayments.sberbank.ru/payment/rest/';
  const API_URI_TEST = 'https://3dsec.sberbank.ru/payment/rest/';

  private $userName = '';
  private $password = '';

  public static function checkSignature( $Order_ID, $Status, $Signature ) {
    $password = "adadadadadadaddasdasdasdasdasdsadsd"; // пароль из ЛК Uniteller
    // проверка подлинности подписи и данных
    return ( $Signature == strtoupper(md5($Order_ID . $Status . $password)) );
  }

  public static function getData($Order_ID) {
    // Параметры могут извлекаться из БД или из других хранилищ данных, либо
    //содержаться внутри кода
    $Shop_ID = "5001300"; // идентификатор точки продажи
    $Login = 1; // логин из ЛК Uniteller
    $Password = "adadadadadadaddasdasdasdasdasdsadsd"; // пароль из ЛК Uniteller
    // Format=1 - получить данные в виде строки с разделителем ";", можно получать
    //данные и в других форматах (см. Технический порядок), например, XML, тогда обработка
    //полученного ответа изменится
    $sPostFields =
      "Shop_ID=".$Shop_ID."&Login=".$Login."&Password=".$Password."&Format=1&ShopOrderNumber=".
      $Order_ID."&S_FIELDS=Status;ApprovalCode;BillNumber";
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://wpay.uniteller.ru/results/");
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_VERBOSE, 0);
    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $sPostFields);
    curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
    curl_setopt($ch, CURLINFO_HEADER_OUT, 1);
    $curl_response = curl_exec($ch);
    $curl_error = curl_error($ch);
    $data = []; // результат для возврата
    if ($curl_error) {
      // обработка ошибки обращения за статусом платежа
    }
    else {
      // данные получены
      // обработка данных из переменной $curl_response
      $arr = explode( ";", $curl_response );
      if ( count($arr) > 2 ) {
        $data = [
          "Status" => $arr[0],
          "ApprovalCode" => $arr[1],
          "BillNumber"  => $arr[2]
        ];
      }
      else {
        // что-то не так, обработчик полученного ответа
      }
    }
    return $data;
  }

}
